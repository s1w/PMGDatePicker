//
//  ViewController.swift
//  PMGDatePicker
//
//  Created by Yevhenii Veretennikov on 3/9/17.
//
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var label: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    @IBAction func openDatePicker() {
        PMGDatePicker().show(doneTitle: "Done", cancelTitle: "Cancel") { (date) in
            self.label.text = date?.with(format: "dd MMMM YYYY") ?? ""
        }
    }

}

