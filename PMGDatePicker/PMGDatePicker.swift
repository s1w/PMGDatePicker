//
//  PMGDatePicker.swift
//  PayMyGift
//
//  Created by Yevhenii Veretennikov on 3/9/17.
//  Copyright © 2017 ensies. All rights reserved.
//

import UIKit

extension Selector {
    static let close = #selector(PMGDatePicker.close)
    static let buttonTapped = #selector(PMGDatePicker.buttonTapped)
}

extension Date {
    func with(format: String) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = format
        return formatter.string(from: self)
    }
}

class PMGDatePicker: UIView {
    
    public typealias PMGDatePickerCallBack = (Date?) -> Void
    
    // MARK: - constants
    
    private let kPMGDatePickerContentViewHeight     : CGFloat = 240
    private let kPMGDatePickerSeparatorLineHeight   : CGFloat = 1
    private let kPMGDatePickerButtonHeight          : CGFloat = 45
    private let kPMGDatePickerButtonWidth           : CGFloat = 90
    private let kPMGDatePickerHeight                : CGFloat = 200
    private let kPMGDataPickerDoneButtonTag         : Int = 1
    
    // MARK: - views
    
    private var doneButton      : UIButton!
    private var cancelButton    : UIButton!
    private var datePicker      : UIDatePicker!
    private var separatorLine   : UIView!
    private var backView        : UIView!
    private var rootView        : UIView!
    
    // MARK: - variables
    
    private var completion      : PMGDatePickerCallBack?
    
    // MARK: - init block
    
    public init() {
        super.init(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height))
        
        self.alpha = 0
        self.setupView()
    }
    
    func setupView() {
        self.backView = UIView()
        self.backView.backgroundColor = UIColor(white: 0, alpha: 0.3)
        self.backView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: Selector.close))
        self.backView.frame = self.frame
        
        self.setupRootView()
        
        self.addSubview(backView)
        self.addSubview(rootView)
        
        UIView.animate(withDuration: 0.2, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            self.rootView.frame = CGRect(x: 0, y: UIScreen.main.bounds.height-self.kPMGDatePickerContentViewHeight, width: UIScreen.main.bounds.width, height: self.kPMGDatePickerContentViewHeight)
            self.alpha = 1
        }, completion:  nil)
    }
    
    func show(doneTitle: String, cancelTitle: String, completion: @escaping PMGDatePickerCallBack) {
        
        self.doneButton.setTitle(doneTitle, for: .normal)
        self.cancelButton.setTitle(cancelTitle, for: .normal)
        self.completion = completion
        
        guard let appDelegate = UIApplication.shared.delegate else { fatalError() }
        guard let window = appDelegate.window else { fatalError() }
        window?.addSubview(self)
        window?.bringSubview(toFront: self)
        window?.endEditing(true)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func close() {
        
        UIView.animate(withDuration: 0.2, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            self.rootView.frame = CGRect(x: 0, y: UIScreen.main.bounds.height, width: UIScreen.main.bounds.width, height: self.kPMGDatePickerContentViewHeight)
            self.alpha = 0
        }) { (finished) in
            for view in self.subviews {
                view.removeFromSuperview()
            }
            self.removeFromSuperview()
        }
        
    }
    
    private func setupRootView() {
        self.rootView = UIView()
        self.rootView.backgroundColor = .white
        self.rootView.frame = CGRect(x: 0, y: UIScreen.main.bounds.height, width: UIScreen.main.bounds.width, height: kPMGDatePickerContentViewHeight)
        
        self.separatorLine = UIView()
        self.separatorLine.backgroundColor = UIColor.gray
        self.separatorLine.frame = CGRect(x: 0, y: kPMGDatePickerButtonHeight, width: UIScreen.main.bounds.width, height: kPMGDatePickerSeparatorLineHeight)
        
        self.datePicker = UIDatePicker()
        self.datePicker.datePickerMode = .date
        self.datePicker.setDate(Date(timeIntervalSince1970: 631152000), animated: true)
        self.datePicker.minimumDate = Date(timeIntervalSince1970: 0)
        self.datePicker.maximumDate = Date(timeIntervalSinceNow: 0)
        self.datePicker.frame = CGRect(x: 0, y: kPMGDatePickerButtonHeight, width: UIScreen.main.bounds.width, height: kPMGDatePickerHeight)
        
        self.doneButton = UIButton(type: .system)
        self.doneButton.tag = kPMGDataPickerDoneButtonTag
        self.doneButton.addTarget(self, action: Selector.buttonTapped, for: .touchUpInside)
        self.doneButton.frame = CGRect(x: UIScreen.main.bounds.width-10-kPMGDatePickerButtonWidth, y: 0, width: kPMGDatePickerButtonWidth, height: kPMGDatePickerButtonHeight)
        
        self.cancelButton = UIButton(type: .system)
        self.cancelButton.addTarget(self, action: Selector.buttonTapped, for: .touchUpInside)
        self.cancelButton.frame = CGRect(x: 10, y: 0, width: kPMGDatePickerButtonWidth, height: kPMGDatePickerButtonHeight)
        
        self.rootView.addSubview(separatorLine)
        self.rootView.addSubview(datePicker)
        self.rootView.addSubview(doneButton)
        self.rootView.addSubview(cancelButton)
    }
    
    func buttonTapped(sender: UIButton!) {
        if sender.tag == kPMGDataPickerDoneButtonTag {
            self.completion?(self.datePicker.date)
        } else {
            self.completion?(nil)
        }
        self.close()
    }
    
}














